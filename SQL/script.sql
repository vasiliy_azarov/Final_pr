-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';


-- Schema finalproject
-- -----------------------------------------------------
DROP DATABASE finalproject;
CREATE SCHEMA IF NOT EXISTS `finalproject` DEFAULT CHARACTER SET utf8 ;
USE `finalproject` ;

-- -----------------------------------------------------
-- Table `finalproject`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `finalproject`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `role` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `finalproject`.`rout`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `finalproject`.`rout` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `way_range` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `finalproject`.`receipt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `finalproject`.`receipt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `price` VARCHAR(45) NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `volume` INT NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `arrive_date` VARCHAR(255) NOT NULL,
  `user_id` INT NOT NULL,
  `rout_id` INT NOT NULL,
  `weight` VARCHAR(45) NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Chek_user1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_receipt_rout1_idx` (`rout_id` ASC) VISIBLE,
  CONSTRAINT `fk_Chek_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `finalproject`.`user` (`id`),
  CONSTRAINT `fk_receipt_rout1`
    FOREIGN KEY (`rout_id`)
    REFERENCES `finalproject`.`rout` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT user(login, password, role) VALUE ('Client','c','user');
INSERT user(login, password, role) VALUE ('Client2','c','user');
INSERT user(login, password, role) VALUE ('Manager','m','Manager');
INSERT rout(name, way_range) VALUE ('Kharkiv-Kiev' , 480);
INSERT rout(name, way_range) VALUE ('Kharkiv-Poltava', 144);
INSERT rout(name, way_range) VALUE ('Kharkiv-Sumy',  185);
INSERT rout(name, way_range) VALUE ('Kharkiv-Zaporizhzhia' , 297);
INSERT rout(name, way_range) VALUE ('Kharkiv-Dnipro' , 128);
INSERT rout(name, way_range) VALUE ('Kharkiv-Chernihiv' , 507);
INSERT rout(name, way_range) VALUE ('Kharkiv-Cherkassy' , 372);
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
