<%--
  Created by IntelliJ IDEA.
  User: Василий
  Date: 18.02.2021
  Time: 16:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>ManagerPage</title>
    <style>
        body {
            padding:0;
            margin:0;
        }
        .btn-01:hover {
            background: #5a6771 !important;
            border-radius: 15px !important;
            width: 280px !important;
            color: #f5f5f6 !important;
        }

        .btn-01 {
            transition: 0.6s;
            font-family: Arial, Helvetica, sans-serif;
        }
        .header {
            letter-spacing:-0.2px;
            transition:0.5s;
        }
        .header:hover {
            letter-spacing:2.2px;
        }
    </style>
</head>
<body style="background: radial-gradient(circle, rgba(104,144,175,1) 0%, rgba(38,45,77,1) 100%);">
<div align="center" class="header" style="background:#56699a; padding:30px; border:2px solid #5a6771;" width="100%" height="10%">
    <a href="#" style="text-decoration:none; font-size:40px; color:#363839; font-weight:700; font-family: Arial, Helvetica, sans-serif;">TRUCK THE WORLD</a>
</div>
<div align="right" style="padding:10px;">
    <form action="Controller" method="post">
        <input type="hidden" name="command" value="logOut">
        <input class="btn-01" type="submit" value="LogOut" style="color:#5a6771; font-size:30px; font-weight:700; border-radius:10px; background:#f5f5f6; width:250px; height:50px;">
    </form>
</div>

<div align="center">

    <form action="ManagerController" method="post">
       <div style="padding:0 0 15px;">
           <span style="color:#4e1645; font-weight:700; font-size:20px; font-family: Arial, Helvetica, sans-serif;">sort by</span>
           <select name="selectSortType" style="height:30px; font-family: Arial, Helvetica, sans-serif; font-size:20px; border-radius:10px;">
               <option>ARRIVAL DATE</option>
               <option>ROUTE</option>
           </select><br>
       </div>
        <input type="hidden" name="command" value="sortList">
        <input class="btn-01" type="submit" value="SORT" style="color:#5a6771; font-size:20px; font-weight:700; border-radius:10px; background:#f5f5f6; width:200px; height:30px;">

    </form>
    <table cellspacing="2" border="1" cellpadding="5" width="1200" style="padding:0 0 5px; color:#363839; font-weight:700; background: linear-gradient(90deg, rgba(195,206,217,1) 0%, rgba(154,196,237,1) 100%); border-radius:8px; font-size:15px; font-family: Arial, Helvetica, sans-serif;">
        <thead>
        <tr>

            <td class="nameColonn">id</td>
            <td class="nameColonn">price</td>
            <td class="nameColonn">status</td>
            <td class="nameColonn">volume</td>
            <td class="nameColonn">address</td>
            <td class="nameColonn">arrive date</td>
            <td class="nameColonn">weight</td>
            <td class="nameColonn">description</td>
            <td class="nameColonn">rout</td>
            <td class="nameColonn">id_user</td>
            <td class="nameColonn">range</td>

        </tr>
        </thead>
        <c:set var="k" value="0"/>
        <c:forEach var="item" items="${receiptList}">
        <c:set var="k" value="${k+1}"/>
        <tr>
            <td>${item.id}</td>
            <td>${item.price}</td>
            <c:if test="${item.status == \"pending\"}">
                <td style="background: #4d92d7;">${item.status}</td>
            </c:if>
            <c:if test="${item.status == \"canceled\"}">
                <td style="background: #9b5668">${item.status}</td>
            </c:if>
            <c:if test="${item.status == \"readyToPay\"}">
                <td style="background: #c4be41">${item.status}</td>
            </c:if>
            <c:if test="${item.status == \"paid\"}">
                <td style="background: #62b171;">${item.status}</td>
            </c:if>


            <td>${item.volume}</td>
            <td>${item.address}</td>
            <td>${item.arrive_date}</td>
            <td>${item.weight}</td>
            <td>${item.description}</td>
            <td>${item.nameRout}</td>
            <td>${item.user_id}</td>
            <td>${item.rangeRout}</td>
            </c:forEach>
        </tr>
    </table>

   <div style="padding:10px 0 0;">
       <c:if test="${pendingList != null}">
           <form action="ManagerController" method="post">
               <select name="select1" style="height:30px; font-family: Arial, Helvetica, sans-serif; font-size:20px; border-radius:10px;" }>
                   <c:set var="k" value="0"/>
                   <c:forEach var="item" items="${pendingList}">
                       <c:set var="k" value="${k+1}"/>
                       <option>${item.id}</option>
                   </c:forEach>
               </select>
               <select name="select2" style="height:30px; font-family: Arial, Helvetica, sans-serif; font-size:20px; border-radius:10px;" }>
                   <option>ACCEPT THE ORDER</option>
                   <option>CANCEL THE ORDER</option>
               </select>
               <br><br>
               <input type="hidden" name="command" value="updateReceiptFromManager">
               <input class="btn-01" type="submit" value="EXECUTE" style="color:#5a6771; font-size:20px; font-weight:700; border-radius:10px; background:#f5f5f6; width:200px; height:30px;">
           </form>
       </c:if>
       <c:if test="${pendingList == null}">
           <span style="color:#4e1645; font-weight:700; font-size:20px; font-family: Arial, Helvetica, sans-serif;">ALL THE RECEIPTS ARE PROCESSED</span>
       </c:if>
   </div>

</div>

<c:if test="${user != null}">

</c:if>

</body>
</html>
