<%--
  Created by IntelliJ IDEA.
  User: Василий
  Date: 18.02.2021
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>LoginPage</title>
    <style>
        input {outline:none;}
        body {
            padding:0;
            margin:0;
        }
        .btn-01:hover{
            background:#5a6771 !important;
            border-radius:15px !important;
            width:280px !important;
            color:#f5f5f6 !important
        }
        .btn-01 {
            transition:0.6s;
            font-family: Arial, Helvetica, sans-serif
        }
        .header {
            letter-spacing:-0.2px;
            transition:0.5s
        }
        .header:hover {
            letter-spacing:2.2px
        }
    </style>
</head>
<body align="center" style="background: radial-gradient(circle, rgba(104,144,175,1) 0%, rgba(38,45,77,1) 100%);">
<div align="center" class="header" style="background:#56699a; padding:30px; border:2px solid #5a6771;" width="100%" height="10%">
    <a href="#" style="text-decoration:none; font-size:40px; color:#363839; font-weight:700; font-family: Arial, Helvetica, sans-serif;">TRUCK THE WORLD</a>
</div>

<div style="padding:100px 0 0;">
    <form action="Controller" method="post">
        <div style="padding:0 0 20px;">
            <input type="hidden" name="command" value="Login">
            <input placeholder="Login" type="text" name="login" style="border-radius:10px; width:150px; height:30px;">
            <br /> <br />
            <input placeholder="Password" type="password" name="password" style="border-radius:10px; width:150px; height:30px;">
        </div>
        <div style="padding:0 0 20px;">
            <input class="btn-01" type="submit" value="Login" style="border:3px solid #5a6771 ; color:#5a6771; font-size:30px; font-weight:700; border-radius:15px; background:#f5f5f6; width:180px; height:50px;">
        </div>
        <span style="color:#4e1645; font-size:25px; text-transform:uppercase; font-family: Arial, Helvetica, sans-serif;"><b>${message}</b></span>
    </form>
</div>
</body>
</html>
