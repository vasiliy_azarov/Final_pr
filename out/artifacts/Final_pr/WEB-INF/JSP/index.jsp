<%--
  Created by IntelliJ IDEA.
  User: Василий
  Date: 13.02.2021
  Time: 13:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>MainPage</title>
    <style>

        body {
            padding:0;
            margin:0;
        }
        input {
            outline: none;
        }

        .btn-01:hover {
            background: #5a6771 !important;
            border-radius: 15px !important;
            width: 280px !important;
            color: #f5f5f6 !important;
        }

        .btn-01 {

            transition: 0.6s;
            font-family: Arial, Helvetica, sans-serif;
        }

        .anim-01 {
            float:left;
            color:#4e1645;
            font-size:25px;
            text-transform:uppercase;
            font-family: Arial, Helvetica, sans-serif;
            animation-duration: 4s;
            animation-name: animka;
            animation-iteration-count: 1;
            animation-fill-mode: forwards;
        }

        @keyframes animka {
            1% {
                margin-left: -1000px;
            }
            100% {
                margin-left: 27%;

            }
        }
        .header {
            letter-spacing:-0.2px;
            transition:0.5s;
        }
        .header:hover {
            letter-spacing:2.2px;
        }
    </style>
</head>
<body style="background: radial-gradient(circle, rgba(104,144,175,1) 0%, rgba(38,45,77,1) 100%);">


<div align="center" class="header" style="background:#56699a; padding:30px; border:2px solid #5a6771;" width="100%" height="10%">
    <a href="#" style="text-decoration:none; font-size:40px; color:#363839; font-weight:700; font-family: Arial, Helvetica, sans-serif;">TRUCK THE WORLD</a>
</div>


<div align="center" style="padding:10% 0 0;">
    <form action="Controller" method="post">
        <input type="hidden" name="command" value="goToPageLogin" style="border-radius:6px;">
        <input class="btn-01" type="submit" value="LOGIN"
               style="border:3px solid #5a6771 ; color:#5a6771; font-size:30px; font-weight:700; border-radius:15px; background:#f5f5f6; width:180px; height:50px;">
    </form>
</div>

<div align="center">
    <form action="Controller" method="post">
        <input type="hidden" name="command" value="goToCalculator" style="border-radius:6px;">
        <input class="btn-01" type="submit" value="Calculate price"
               style="color:#5a6771; font-size:30px; font-weight:700; border-radius:10px; background:#f5f5f6; width:250px; height:50px;">
    </form>
</div>

<div class="anim-01">
    <table>
        <tr>
            <td style="color: #060c56;
            font-size:25px;
            text-transform:uppercase;
            font-family: Arial, Helvetica, sans-serif;">
                TRUCK THE WORLD company is engaged in cargo <br>
                transportation all over Ukraine. You can calculate<br>
                the cost of delivery of the goods in the direction<br>
                you need by going to the page "Calculate price".
            </td>
            <td>
                <img style="float:right" src="https://img.icons8.com/carbon-copy/2x/truck.png" alt="GRUZOVIK ">
            </td>
        </tr>
    </table>
</div>


</form>
</body>
</html>
