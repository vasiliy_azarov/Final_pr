<%--
  Created by IntelliJ IDEA.
  User: Василий
  Date: 21.02.2021
  Time: 15:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>CalculatorPage</title>
    <style>
        body {
            padding: 0;
            margin: 0;
        }

        input {
            outline: none;
        }

        .btn-01:hover {
            background: #5a6771 !important;
            border-radius: 15px !important;
            width: 280px !important;
            color: #f5f5f6 !important;
        }

        .btn-01 {
            transition: 0.6s;
            font-family: Arial, Helvetica, sans-serif;
        }

        .header {
            letter-spacing: -0.2px;
            transition: 0.5s;
        }

        .header:hover {
            letter-spacing: 2.2px;
        }
    </style>
</head>
<body align="center" style="background: radial-gradient(circle, rgba(104,144,175,1) 0%, rgba(38,45,77,1) 100%);">

<div align="center" class="header" style="background:#56699a; padding:30px; border:2px solid #5a6771;" width="100%"
     height="10%">
    <a href="#"
       style="text-decoration:none; font-size:40px; color:#363839; font-weight:700; font-family: Arial, Helvetica, sans-serif;">TRUCK
        THE WORLD</a>
</div>

<div style="padding:30px;">
    <c:if test="${user == null}">
        <form action="Controller" method="post">
            <input type="hidden" name="command" value="goToPageLogin">
            <input class="btn-01" type="submit" value="Login"
                   style="border:3px solid #5a6771 ; color:#5a6771; font-size:30px; font-weight:700; border-radius:15px; background:#f5f5f6; width:180px; height:50px;">
        </form>

    </c:if>
    <c:if test="${user != null}">
        <form action="Controller" method="post">
            <input class="btn-01" type="hidden" name="command" value="logOut">
            <input class="btn-01" type="submit" value="LogOut"
                   style="border:3px solid #5a6771 ; color:#5a6771; font-size:30px; font-weight:700; border-radius:15px; background:#f5f5f6; width:180px; height:50px;">
        </form>
        <form action="Controller" method="post">
            <input type="hidden" name="command" value="goToBasket">
            <input class="btn-01" type="submit" value="BASKET"
                   style="border:3px solid #5a6771 ; color:#5a6771; font-size:30px; font-weight:700; border-radius:15px; background:#f5f5f6; width:180px; height:50px;">
        </form>
    </c:if>

    <form>
        <input type="hidden" name="command" value="Calculate">
        <table align="center">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <span style="font-size:20px;">height:</span><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="number" value="ВЫСОТА" name="height" min="1"
                                       style="border:3px solid #56699a; border-radius:10px; width:150px; height:30px;">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <span style="font-size:20px;">length:</span><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="number" name="length" min="1"
                                       style="border:3px solid #56699a; border-radius:10px; width:150px; height:30px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table align="center">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <span style="font-size:20px;">width</span><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="number" name="width" min="1"
                                       style="border:3px solid #56699a; border-radius:10px; width:150px; height:30px;">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <span style="font-size:20px;">weight</span><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="number" name="weight" min="1"
                                       style="border:3px solid #56699a; border-radius:10px; width:150px; height:30px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table align="center">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <span style="font-size:20px;">address</span><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="address" min="1"
                                       style="border:3px solid #56699a; border-radius:10px; width:150px; height:30px;">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <span style="font-size:20px;">choiceData:</span><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="date" name="choiceData"
                                       style="border:3px solid #56699a; border-radius:10px; width:150px; height:30px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table align="center">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <span style="font-size:20px;">description</span><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input autocomplete="off" type="text" name="description"
                                       style="border:3px solid #56699a; border-radius:10px; width:150px; height:30px;">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <span style="font-size:20px;">route</span> <br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select name="select"
                                        style="border:3px solid #56699a; border-radius:10px; font-size:15px; height:30px;"
                                        }>
                                    <c:set var="k" value="0"/>
                                    <c:forEach var="item" items="${routList}">
                                        <c:set var="k" value="${k+1}"/>
                                    <option>${item.name}</option>

                                    </c:forEach>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        </select>
        <br><br>
        <input class="btn-01" type="submit" value="CALCULATE"
               style="border:3px solid #5a6771 ; color:#5a6771; font-size:30px; font-weight:700; border-radius:15px; background:#f5f5f6; width:200px; height:50px;">
    </form>


    <div align="center"
         style="color:#4e1645; font-size:25px; text-transform:uppercase; font-family: Arial, Helvetica, sans-serif;">
        ${message}
        <c:if test="${currentReceipt != null}">
            <table cellspacing="2" border="1" cellpadding="5" width="1200"
                   style="padding:0 0 5px; color:#363839; width: 700px; font-weight:400; background: linear-gradient(90deg, rgba(195,206,217,1) 0%, rgba(154,196,237,1) 100%); border-radius:8px; font-size:15px; font-family: Arial, Helvetica, sans-serif;">
                <tr>
                    <td>
                        price
                    </td>
                    <td>
                            ${currentReceipt1}
                    </td>
                </tr>
                <tr>
                    <td>
                        volume
                    </td>
                    <td>
                            ${currentReceipt3}
                    </td>
                </tr>
                <tr>
                    <td>
                        address
                    </td>
                    <td>
                            ${currentReceipt4}
                    </td>
                </tr>
                <tr>
                    <td>
                        date
                    </td>
                    <td>
                            ${currentReceipt5}
                    </td>
                </tr>

                <tr>
                    <td>
                        weight
                    </td>
                    <td>
                            ${currentReceipt6}
                    </td>
                </tr>
                <tr>
                    <td>
                        discription
                    </td>
                    <td>
                            ${currentReceipt7}
                    </td>
                </tr>
            </table>
            <br>
            <c:if test="${user != null}">
                <form action="UserController" method="post">
                    <input class="btn-01" type="hidden" name="command" value="buyTicket"
                           style="border:3px solid #5a6771 ; color:#5a6771; font-size:30px; font-weight:700; border-radius:15px; background:#f5f5f6; width:180px; height:50px;">
                    <input class="btn-01" type="submit" value="ORDER"
                           style="border:3px solid #5a6771 ; color:#5a6771; font-size:30px; font-weight:700; border-radius:15px; background:#f5f5f6; width:180px; height:50px;">
                </form>
            </c:if>
            <c:if test="${user == null}">
                LOGIN TO ORDER A DELIVERY
            </c:if>
        </c:if>
    </div>
    <c:if test="${user != null}">
        <c:if test="${ticket != null}">
            <form>
                <input type="hidden" name="command" value="OrderTicket">
                <input c type="submit" value="BUY"
                       style="border:3px solid #5a6771 ; color:#5a6771; font-size:30px; font-weight:700; border-radius:15px; background:#f5f5f6; width:180px; height:50px;">
            </form>

        </c:if>

    </c:if>
</div>


</body>
</html>
