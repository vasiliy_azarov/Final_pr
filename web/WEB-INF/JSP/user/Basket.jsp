<%--
  Created by IntelliJ IDEA.
  User: Василий
  Date: 20.02.2021
  Time: 12:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Basket</title>
    <style>
        body {
            padding:0;
            margin:0;
        }
        .btn-01:hover {
            background: #5a6771 !important;
            border-radius: 15px !important;
            width: 280px !important;
            color: #f5f5f6 !important;
        }

        .btn-01 {
            transition: 0.6s;
            font-family: Arial, Helvetica, sans-serif;
        }
        .header {
            letter-spacing:-0.2px;
            transition:0.5s;
        }
        .header:hover {
            letter-spacing:2.2px;
        }
    </style>
</head>
<body style="background: radial-gradient(circle, rgba(104,144,175,1) 0%, rgba(38,45,77,1) 100%);">
<div align="center" class="header" style="background:#56699a; padding:30px; border:2px solid #5a6771;" width="100%" height="10%">
    <a href="#" style="text-decoration:none; font-size:40px; color:#363839; font-weight:700; font-family: Arial, Helvetica, sans-serif;">TRUCK THE WORLD</a>
</div>
<div align="center" style="padding:20px 0 30px;">
    <form action="Controller" method="post">
        <input type="hidden" name="command" value="goToCalculator">
        <input class="btn-01" type="submit" value="< Back" style="color:#5a6771; font-size:30px; font-weight:700; border-radius:10px; background:#f5f5f6; width:250px; height:50px;">
    </form>
</div>
<div align="center">
    <div style="color:#4e1645; font-size:25px; text-transform:uppercase; font-family: Arial, Helvetica, sans-serif;">
<c:if test="${receiptList == null}">
    THE BASKET IS EMPTY
</c:if>
<c:if test="${receiptList != null}">
    </div>
    <table cellspacing="2" border="3" cellpadding="5" width="1200" id="table" style="color:#363839; font-weight:700; background: linear-gradient(90deg, rgba(195,206,217,1) 0%, rgba(154,196,237,1) 100%); border-radius:8px; font-size:15px; font-family: Arial, Helvetica, sans-serif;">
        <thead>
        <tr>
            <td class="nameColonn">id</td>
            <td class="nameColonn">price</td>
            <td class="nameColonn">status</td>
            <td class="nameColonn">volume</td>
            <td class="nameColonn">address</td>
            <td class="nameColonn">arrive date</td>
            <td class="nameColonn">weight</td>
            <td class="nameColonn">description</td>
            <td class="nameColonn">rout</td>
            <td class="nameColonn">range</td>

        </tr>
        </thead>
        <c:set var="k" value="0"/>
        <c:forEach var="item" items="${receiptList}">
        <c:set var="k" value="${k+1}"/>
        <tr>
            <td>${item.id}</td>
            <td>${item.price}</td>
            <c:if test="${item.status == \"pending\"}">
                <td style="background: #4d92d7;">${item.status}</td>
            </c:if>
            <c:if test="${item.status == \"canceled\"}">
                <td style="background: #9b5668">${item.status}</td>
            </c:if>
            <c:if test="${item.status == \"readyToPay\"}">
                <td style="background: #c4be41">${item.status}</td>
            </c:if>
            <c:if test="${item.status == \"paid\"}">
                <td style="background: #62b171;">${item.status}</td>
            </c:if>


            <td>${item.volume}</td>
            <td>${item.address}</td>
            <td>${item.arrive_date}</td>
            <td>${item.weight}</td>
            <td>${item.description}</td>
            <td>${item.nameRout}</td>
            <td>${item.rangeRout}</td>
            </c:forEach>
        </tr>
    </table>


    <div style="padding:15px;">
        <c:if test="${listID != null}">
            <form action="UserController" method="post">
               <div style=" padding:0 0 15px;">
                   <span style="color:#4e1645; font-weight:700; font-size:20px; font-family: Arial, Helvetica, sans-serif;">Please, select receipt to pay </span>
                   <select name="select" style="height:30px; border-radius:5px;" }>
                       <c:set var="k" value="0"/>
                       <c:forEach var="item" items="${listID}">
                           <c:set var="k" value="${k+1}"/>
                           <option>${item.id}</option>
                       </c:forEach>
                   </select>
               </div>
                <br>
                <input type="hidden" name="command" value="updateReceiptFromUser">
                <input class="btn-01" type="submit" value="PAY" style="color:#5a6771; font-size:30px; font-weight:700; border-radius:10px; background:#f5f5f6; width:250px; height:50px;">
            </form>
        </c:if>

        <div style="color:#4e1645; font-weight:700; font-size:20px; font-family: Arial, Helvetica, sans-serif;">
            <c:if test="${listID == null}">
                NO DELIVERIES PREPARED FOR PAYMENT
            </c:if>

            </c:if>
    </div>
    </div>
</div>
</body>
</html>
