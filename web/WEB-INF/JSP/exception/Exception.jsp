<%--
  Created by IntelliJ IDEA.
  User: Василий
  Date: 13.02.2021
  Time: 13:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>ExceptionPage</title>
    <style>

        body {
            padding: 0;
            margin: 0;
        }

        input {
            outline: none;
        }

        .btn-01:hover {
            background: #5a6771 !important;
            border-radius: 15px !important;
            width: 280px !important;
            color: #f5f5f6 !important;
        }

        .btn-01 {
            transition: 0.6s;
            font-family: Arial, Helvetica, sans-serif;
        }

        .anim-01 {
            animation-duration: 7s;
            animation-name: animka;
            animation-iteration-count: infinite;
        }

        @keyframes animka {
            1% {
                margin-left: -500px;
            }
            50% {
                margin-left: 1900px;
            }
            100% {
                margin-left: -500px;
            }
        }

        .header {
            letter-spacing: -0.2px;
            transition: 0.5s;
        }

        .header:hover {
            letter-spacing: 2.2px;
        }
    </style>
</head>
<body style="background: radial-gradient(circle, rgba(104,144,175,1) 0%, rgba(38,45,77,1) 100%);">


<div align="center" class="header" style="background:#56699a; padding:30px; border:2px solid #5a6771;" width="100%"
     height="10%">
    <a href="#"
       style="text-decoration:none; font-size:40px; color:#363839; font-weight:700; font-family: Arial, Helvetica, sans-serif;">TRUCK
        THE WORLD</a>
</div>
<div align="center" style="margin-top: 30px;">


    <span style="color:#4e1645; font-size:35px; text-transform:uppercase; font-family: Arial, Helvetica, sans-serif;"><b>${message}</b></span>
</div>


</form>
</body>
</html>
