package web;

public class Path {
    public static final String PAGE_CALCULATOR = "/WEB-INF/JSP/calculator.jsp";
    public static final String PAGE_BASKET = "/WEB-INF/JSP/user/Basket.jsp";
    public static final String PAGE_LOGIN = "/WEB-INF/JSP/LoginPage.jsp";
    public static final String PAGE_MANAGER = "/WEB-INF/JSP/manager/managerMain.jsp";
    public static final String PAGE_MAIN =  "/WEB-INF/JSP/index.jsp";
    public static final String PAGE_EXCEPTION = "/WEB-INF/JSP/exception/Exception.jsp";
}
