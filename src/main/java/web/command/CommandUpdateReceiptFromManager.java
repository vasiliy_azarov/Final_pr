package web.command;

import db.DBManager;
import db.entity.Receipt;
import exception.dbException;
import org.apache.log4j.Logger;
import web.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommandUpdateReceiptFromManager extends Command {
    /**
     * CommandUpdateReceiptFormUser command.
     * This command puts changes in receipts made by manager
     * @author V.Azarov
     */
    private static final Logger LOG = Logger.getLogger(CommandUpdateReceiptFromManager.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, dbException {
        request.getSession().setAttribute("currentReceipt", null);
        if (  request.getSession().getAttribute("user") == null){

            String forward = Path.PAGE_MAIN;
            return forward;
        }
        String forward = Path.PAGE_MANAGER;

        int id = Integer.valueOf(request.getParameter("select1"));
        String command = request.getParameter("select2");
        if ("ACCEPT THE ORDER".equals(command)) {
            DBManager.getInstance().updateStatus(id, "readyToPay");
        } else {
            DBManager.getInstance().updateStatus(id, "canceled");
        }
        request.setAttribute("receiptList", DBManager.getInstance().getReceiptList());
        List<Receipt> pendingList = new ArrayList<>();
        for (Receipt r : DBManager.getInstance().getReceiptList()) {
            if (r.getStatus().equals("pending")) {
                pendingList.add(r);
            }
        }
        if (pendingList.size() == 0) {
            pendingList = null;
        }
        request.setAttribute("pendingList", pendingList);
        LOG.info("Update receipt by Manager");
        return forward;
    }
}
