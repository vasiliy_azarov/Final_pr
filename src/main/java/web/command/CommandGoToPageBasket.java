package web.command;

import db.DBManager;
import db.entity.Receipt;
import db.entity.User;
import exception.dbException;
import org.apache.log4j.Logger;
import web.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommandGoToPageBasket extends Command{
    /**
     * CommandGoToPageBasket command.
     * This command navigates to Basket page
     * @author V.Azarov
     */
    private static final Logger LOG = Logger.getLogger(CommandGoToPageBasket.class);
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, dbException {
        request.getSession().setAttribute("currentReceipt", null);

        User user = (User) request.getSession().getAttribute("user");
        List<Receipt> receiptList = DBManager.getInstance().getReceiptList();
        List<Receipt> list2 = new ArrayList<>();
        for (Receipt r : receiptList){
            if( r.getUser_id() == user.getId()){
                list2.add(r);
            }
        }
        List<Receipt> list3 = new ArrayList<>();

        for (Receipt r : list2){
           if ("readyToPay".equals(r.getStatus())){
               list3.add(r);
           }
        }
        if (list3.size()!= 0){
            request.setAttribute("listID" , list3 );
        }
        if (list2.size()!= 0){
            request.setAttribute("receiptList" , list2 );
        }


        String forward = Path.PAGE_BASKET;
        LOG.info("GO TO BASKET PAGE");
        return forward;
    }
}
