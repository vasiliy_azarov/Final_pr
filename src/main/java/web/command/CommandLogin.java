package web.command;

import db.DBManager;
import db.entity.Receipt;
import db.entity.Rout;
import db.entity.User;
import exception.dbException;
import org.apache.log4j.Logger;
import web.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommandLogin extends Command{
    /**
     * CommandLogin command.
     * This command authorizes the user
     *  and redirects to the main page depending on the role.
     * @author V.Azarov
     *
     */

    private static final Logger LOG = Logger.getLogger(CommandLogin.class);
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException, dbException {
        request.getSession().setAttribute("currentReceipt", null);

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        List<User> userList = DBManager.getInstance().getUserList();
        User user = null;
        for (int i = 0 ; i < userList.size() ; i ++){
            if (login.equals(userList.get(i).getLogin())){
                user = userList.get(i);
                break;
            }
        }
        if(user == null){
            String forward = Path.PAGE_LOGIN;
            request.setAttribute("message" , "USER IS NOT FOUND");
            LOG.error("USER IS NOT FOUND");
            return forward;
        }

        if (user.getPassword().equals(password)){
            request.getSession().setAttribute("user" ,user);
            String role = user.getRole();
            if ("Manager".equals(role)){

                request.setAttribute("receiptList", DBManager.getInstance().getReceiptList());
                List<Receipt> pendingList = new ArrayList<>();
                for ( Receipt r : DBManager.getInstance().getReceiptList()){
                    if (r.getStatus().equals("pending")){
                        pendingList.add(r);
                    }
                }
                if (pendingList.size() != 0){
                    request.setAttribute("pendingList",pendingList);

                }
                String forward = Path.PAGE_MANAGER;
                LOG.info("GO TO MANAGER PAGE");

                return forward;
            }
            List<Rout> routList = DBManager.getInstance().getRoutList();
            request.setAttribute("routList", routList);
            String forward = Path.PAGE_CALCULATOR;
            LOG.info("GO TO CALCULATOR PAGE (USER)");

            return forward;
        } else {
            String forward = Path.PAGE_LOGIN;
            request.setAttribute("message" , "INCORRECT PASSWORD");
            LOG.error("INCORRECT PASSWORD");

            return forward;
        }
    }
}
