package web.command;

import org.apache.log4j.Logger;
import web.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CommandLogOut extends Command{
    /**
     * CommandLogout command.
     * This command out of account.
     * @author V.Azarov
     *
     */

    private static final Logger LOG = Logger.getLogger(CommandLogOut.class);
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getSession().setAttribute("currentReceipt", null);

        request.getSession().setAttribute("user" , null);
        String forward = Path.PAGE_MAIN;
        LOG.info("LogOut");

        return forward;
    }
}
