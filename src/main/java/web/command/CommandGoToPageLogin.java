package web.command;

import org.apache.log4j.Logger;
import web.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CommandGoToPageLogin extends Command{
    /**
     * CommandGoToPageLogin command.
     * This command navigates to Login page
     * @author V.Azarov
     */
    private static final Logger LOG = Logger.getLogger(CommandGoToPageLogin.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getSession().setAttribute("currentReceipt", null);

        String forward = Path.PAGE_LOGIN;
        LOG.info("GO TO LOGIN PAGE");
        return forward;
    }
}
