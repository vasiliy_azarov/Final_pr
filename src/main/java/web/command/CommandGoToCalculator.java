package web.command;

import db.DBManager;
import db.entity.Rout;
import exception.dbException;
import org.apache.log4j.Logger;
import web.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CommandGoToCalculator extends Command{
    /**
     * CommandGoToCalculator command.
     * This command navigates to Calculator page
     * @author V.Azarov
     */
    private static final Logger LOG = Logger.getLogger(CommandGoToCalculator.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, dbException {
        request.getSession().setAttribute("currentReceipt", null);

        List<Rout> routList = DBManager.getInstance().getRoutList();
        request.setAttribute("routList", routList);
        String forward = Path.PAGE_CALCULATOR;
        LOG.info("GO TO CALCULATOR PAGE");
        return forward;
    }
}
