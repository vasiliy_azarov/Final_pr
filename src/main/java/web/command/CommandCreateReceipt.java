package web.command;

import db.DBManager;
import db.entity.Receipt;
import db.entity.User;
import exception.dbException;
import org.apache.log4j.Logger;
import web.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CommandCreateReceipt extends Command{
    /**
     * CommandCreateReceipt command.
     * This command posts current receipt on page
     * @author V.Azarov
     */

    private static final Logger LOG = Logger.getLogger(CommandCreateReceipt.class);
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, dbException {
        HttpSession session = request.getSession();
        Receipt receipt = (Receipt) session.getAttribute("currentReceipt");
        if (receipt == null){
            String forward = Path.PAGE_CALCULATOR;
            request.getSession().setAttribute("currentReceipt", null);
            request.setAttribute("routList", DBManager.getInstance().getRoutList());
            LOG.info("CHECK POSTED ON PAGE");
            return forward;
        }
        session.setAttribute("currentReceipt" , null);
        User user = (User) session.getAttribute("user");
        receipt.setUser_id(user.getId());
        DBManager.getInstance().addReceiptToBase(receipt);
        String message = "THE REQUEST FOR DELIVERY IS CREATED";
        request.setAttribute("message", message);
        String forward = Path.PAGE_CALCULATOR;
        request.getSession().setAttribute("currentReceipt", null);
        request.setAttribute("routList", DBManager.getInstance().getRoutList());
        LOG.info("RECEIPT POSTED ON PAGE TO USER");
        return forward;
    }
}
