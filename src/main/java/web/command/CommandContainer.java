package web.command;

import java.util.Map;
import java.util.TreeMap;
/**
 * Holder for all commands.<br/>
 *
 * @author V.Azarov
 *
 */


public class CommandContainer {

    private static Map<String, Command> commands = new TreeMap<String, Command>();

    static {
        // commands
        commands.put("goToPageLogin", new CommandGoToPageLogin());
        commands.put("Login", new CommandLogin());
        commands.put("Calculate", new CommandCalculator());
        commands.put("goToBasket", new CommandGoToPageBasket());
        commands.put("goToCalculator", new CommandGoToCalculator());
        commands.put("buyTicket", new CommandCreateReceipt());
        commands.put("logOut", new CommandLogOut());
        commands.put("updateReceiptFromManager", new CommandUpdateReceiptFromManager());
        commands.put("updateReceiptFromUser", new CommandUpdateReceiptFormUser());
        commands.put("sortList" , new CommandSort());


    }
    /**
     * Returns command object with the given name.
     *
     * @param commandName
     *            Name of the command.
     * @return Command object.
     */
    public static Command get(String commandName) {
        if (commandName == null || !commands.containsKey(commandName)) {
            return commands.get("noCommand");
        }

        return commands.get(commandName);
    }
}