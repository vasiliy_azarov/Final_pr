package web.command;

import db.DBManager;
import db.entity.Receipt;
import db.entity.Rout;
import exception.dbException;
import org.apache.log4j.Logger;
import web.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CommandCalculator extends Command {
    /**
     * CommandCalculator command.
     * This command processes the input data,
     * calculates price of delivery
     * and includes it in data base.
     * @author V.Azarov
     */

    private static final Logger LOG = Logger.getLogger(CommandCalculator.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, dbException {
        String forward = Path.PAGE_CALCULATOR;
        int height;
        int length;
        int width;
        int weight;
        String address;
        String way;
        String date;
        String description;

        try {
            height = Integer.valueOf(request.getParameter("height"));
            length = Integer.valueOf(request.getParameter("length"));
            width = Integer.valueOf(request.getParameter("width"));
            weight = Integer.valueOf(request.getParameter("weight"));
            address = request.getParameter("address");
            way = request.getParameter("select");
            date = (request.getParameter("choiceData"));
            description = request.getParameter("description");
            if (date.equals("")){
                throw new Exception();
            }
            if (address.length() > 45){
                throw new Exception();
            }
        } catch (Exception e) {
            LOG.error("data entry error");

            request.setAttribute("routList", DBManager.getInstance().getRoutList());
            request.getSession().setAttribute("currentReceipt", null);
            String V = "incorrect input";
            request.setAttribute("message", V);
            LOG.error("data entry error");
            return forward;
        }
        request.setAttribute("routList", DBManager.getInstance().getRoutList());

        List<Rout> routList2 = DBManager.getInstance().getRoutList();
        Rout rout2 = null;
        for (int i = 0; i < routList2.size(); i ++){
            if (routList2.get(i).getName().equals(way)){
                rout2 = routList2.get(i);
            }
        }
        rout2.getRange();


        int V = height * length * width;
        int price = (weight+V+rout2.getRange())*21;


        List<Rout> routList = DBManager.getInstance().getRoutList();
        Rout rout = null;
        for(Rout r : routList){
            if (r.getName().equals(way)){
                rout = r;
            }
        }
        for (int i = 0; i < routList.size(); i ++){
            if (routList.get(i).getName().equals(way)){
                rout = routList.get(i);
            }
        }

        Receipt receipt = new Receipt(String.valueOf(price), "pending", V, address,date,rout.getId() , String.valueOf(weight) , description);
        request.getSession().setAttribute("currentReceipt", receipt);
        request.setAttribute("currentReceipt1", receipt.getPrice());
        request.setAttribute("currentReceipt2", receipt.getStatus());
        request.setAttribute("currentReceipt3", receipt.getVolume());
        request.setAttribute("currentReceipt4", receipt.getAddress());
        request.setAttribute("currentReceipt5", receipt.getArrive_date());
        request.setAttribute("currentReceipt6", receipt.getWeight());
        request.setAttribute("currentReceipt7", receipt.getDescription());
        LOG.info("data entered in the receipt");
        return forward;

    }
}