package web.command;

import db.DBManager;
import db.entity.Receipt;
import db.entity.comporators.DateComporator;
import db.entity.comporators.RoutComporator;
import exception.dbException;
import org.apache.log4j.Logger;
import web.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommandSort extends Command{
    /**
     * CommandSort command.
     * This command sorts receipts on Manager`s page
     * by date and rout
     * @author V.Azarov
     */
    private static final Logger LOG = Logger.getLogger(CommandSort.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, dbException {
        String forward = Path.PAGE_MANAGER;
        List<Receipt> receiptList = DBManager.getInstance().getReceiptList();

        List<Receipt> pendingList = new ArrayList<>();
        for ( Receipt r : receiptList){
            if (r.getStatus().equals("pending")){
                pendingList.add(r);
            }
        }
        if (pendingList.size() != 0){
            request.setAttribute("pendingList",pendingList);

        }

        RoutComporator routComporator = new RoutComporator();
        DateComporator dateComporator = new DateComporator();
        String type = request.getParameter("selectSortType");
        if ("ARRIVAL DATE".equals(type)){
            Collections.sort(receiptList , dateComporator);
        } else {
            Collections.sort(receiptList , routComporator);
        }
        request.setAttribute("receiptList", receiptList);
        LOG.info("Sort receiptList");

        return forward;
    }
}
