package web.controllers;

import web.Path;
import web.command.Command;
import web.command.CommandContainer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Servlet  User controller.
 *
 * @author V.Azarov
 *
 */
@WebServlet("/UserController")
public class UserController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }
    /**
     * Main method of this controller.
     */
    protected void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String commandName = req.getParameter("command");
        Command com = CommandContainer.get(commandName);
        String forward;
        try {
            forward = com.execute(req, resp);
        } catch (Exception ex) {
            forward = Path.PAGE_EXCEPTION;
            req.setAttribute("message", ex.getMessage());
        }
        req.getRequestDispatcher(forward).forward(req, resp);
    }
}
