package db.entity;

import db.DBManager;
import exception.dbException;

import java.util.List;
/**
 * Receipt entity.
 *
 * @author V.Azarov
 *
 */
public class Receipt {
    private int id;
    private String price;
    private String status;
    private int volume;
    private String address;
    private String arrive_date;
    private int user_id;
    private int rout_id;
    private String weight;
    private String description;
    private String nameRout;
    private String rangeRout;



    public Receipt(int id, String price, String status, int volume, String address, String arrive_date, int user_id, int rout_id, String weight, String description) throws dbException {
        this.id = id;
        this.price = price;
        this.status = status;
        this.volume = volume;
        this.address = address;
        this.arrive_date = arrive_date;
        this.user_id = user_id;
        this.rout_id = rout_id;
        this.weight = weight;
        this.description = description;
        List<Rout> routList = DBManager.getInstance().getRoutList();
        Rout rout = null;
        for (Rout r : routList){
            if (r.getId() == rout_id){
                rout = r;
            }
        }
        this.nameRout = rout.getName();
        this.rangeRout = String.valueOf(rout.getRange());
    }

    public Receipt(String price, String status, int volume, String address, String arrive_date, int user_id, int rout_id, String weight, String description) throws dbException {
        this.price = price;
        this.status = status;
        this.user_id = user_id;
        this.rout_id = rout_id;
        this.volume = volume;
        this.address = address;
        this.arrive_date = arrive_date;
        this.weight = weight;
        this.description = description;
        List<Rout> routList = DBManager.getInstance().getRoutList();
        Rout rout = null;
        for (Rout r : routList){
            if (r.getId() == rout_id){
                rout = r;
            }
        }
        this.nameRout = rout.getName();
        this.rangeRout = String.valueOf(rout.getRange());
    }
    public Receipt(String price, String status, int volume, String address, String arrive_date, int rout_id, String weight, String description) throws dbException {
        this.price = price;
        this.status = status;
        this.rout_id = rout_id;
        this.volume = volume;
        this.address = address;
        this.arrive_date = arrive_date;
        this.weight = weight;
        this.description = description;
        List<Rout> routList = DBManager.getInstance().getRoutList();
        Rout rout = null;
        for (Rout r : routList){
            if (r.getId() == rout_id){
                rout = r;
            }
        }
        this.nameRout = rout.getName();
        this.rangeRout = String.valueOf(rout.getRange());
    }

    public String getNameRout() {
        return nameRout;
    }

    public String getRangeRout() {
        return rangeRout;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArrive_date() {
        return arrive_date;
    }

    public void setArrive_date(String arrive_date) {
        this.arrive_date = arrive_date;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getRout_id() {
        return rout_id;
    }

    public void setRout_id(int rout_id) {
        this.rout_id = rout_id;
    }

    @Override
    public String toString() {
        return "Receipt{" +
                "id=" + id +
                ", price='" + price + '\'' +
                ", status='" + status + '\'' +
                ", volume=" + volume +
                ", address='" + address + '\'' +
                ", arrive_date='" + arrive_date + '\'' +
                ", user_id=" + user_id +
                ", rout_id=" + rout_id +
                ", weight='" + weight + '\'' +
                ", description='" + description + '\'' +
                ", nameRout='" + nameRout + '\'' +
                ", rangeRout='" + rangeRout + '\'' +
                '}';
    }


}

