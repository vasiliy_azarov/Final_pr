package db.entity;
/**
 * Route entity.
 *
 * @author V.Azarov
 *
 */
public class Rout {

    private String name;
    private int range;
    private int id;


    public Rout(int id, String name, int range) {

        this.id = id;
        this.name = name;
        this.range = range;
    }

    public Rout(String name, int range) {

        this.name = name;
        this.range = range;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public int getRange(){
        return range;
    }
    public void setRange(int range) {
        this.range = range;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", range='" + range + '\'' +
                ", id=" + id +
                '}';
    }
}
