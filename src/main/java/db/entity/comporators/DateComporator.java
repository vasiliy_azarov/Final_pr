package db.entity.comporators;

import db.entity.Receipt;

import java.util.Comparator;
/**
 * Date Comparator
 *
 * @author V.Azarov
 *
 */
public class DateComporator implements Comparator<Receipt> {
    @Override
    public int compare(Receipt o1, Receipt o2) {
        return o1.getArrive_date().hashCode() - o2.getArrive_date().hashCode();
    }
}
