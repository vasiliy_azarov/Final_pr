package db.entity.comporators;

import db.entity.Receipt;

import java.util.Comparator;
/**
 * Route Comparator
 *
 * @author V.Azarov
 *
 */
public class RoutComporator implements Comparator<Receipt> {

    @Override
    public int compare(Receipt o1, Receipt o2) {
        return o1.getNameRout().hashCode() - o2.getNameRout().hashCode();
    }
}
