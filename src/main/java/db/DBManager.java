package db;

import db.entity.Receipt;
import db.entity.Rout;
import db.entity.User;
import exception.dbException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DB manager. Works with Apache Derby DB.
 *
 * @author V.Azarov
 *
 */
public class DBManager {
    final static private String LOGIN = "root";
    final static String PASSWORD = "Dfcz80509230175";
    private static final String connectionUrl = "jdbc:mysql://localhost/finalProject?serverTimezone=Europe/Moscow&useSSL=false";

    static private DBManager instance;

    public DBManager() {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }
/**
 * getUser list from DataBase
 *@return list
 *        User List
*/

    public List<User> getUserList() throws dbException {
        List<User> list = new ArrayList<>();
        try {


            Connection connection = DriverManager.getConnection(connectionUrl, LOGIN, PASSWORD);
            Statement statement = connection.createStatement();
            ResultSet res = statement.executeQuery("SELECT * FROM finalproject.user;");
            while (res.next()) {
                list.add(new User(res.getInt(1), res.getString(2), res.getString(3), res.getString(4)));
            }
            res.close();
            statement.close();
            connection.close();
        } catch (Exception throwable) {
            throwable.printStackTrace();
            throw new dbException("Error DataBase connectION");
        }
        return list;
    }
    /**
     * add route to DataBase
     *
     */
    public void addRoutToBase(Rout rout) throws dbException {
        String SQL = "INSERT Rout(id, name, 'way_range') VALUE ('" + rout.getId() + "' , '" + rout.getName() + "' , '" + rout.getRange() + "')";
        try {
            Connection connection = DriverManager.getConnection(connectionUrl, LOGIN, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(SQL);
            statement.execute();
            statement.close();
            connection.close();

        } catch (Exception throwable) {
            throwable.printStackTrace();
            throw new dbException("Error DataBase connectION, Table Route");
        }
    }
    /**
     * getRoutelist from DataBase
     *@return list
     *        route List
     */
    public List<Rout> getRoutList() throws dbException {
        List<Rout> list = new ArrayList<>();
        try {


            Connection connection = DriverManager.getConnection(connectionUrl, LOGIN, PASSWORD);
            Statement statement = connection.createStatement();
            ResultSet res = statement.executeQuery("SELECT * FROM finalproject.rout;");
            while (res.next()) {
                list.add(new Rout(res.getInt(1), res.getString(2), res.getInt(3)));
            }
            res.close();
            statement.close();
            connection.close();
        } catch (Exception throwables) {
            throwables.printStackTrace();
            throw new dbException("Error DataBase connectION, Table Route");
        }
        return list;
    }
    /**
     * add User to DataBase
     */
    public void addUserToBase(User user) throws dbException {
        String SQL = "INSERT User(login, password, role) VALUE ('" + user.getLogin() + "' , '" + user.getPassword() + "', '" + user.getRole() + "')";
        try {
            Connection connection = DriverManager.getConnection(connectionUrl, LOGIN, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(SQL);
            statement.execute();
            statement.close();
            connection.close();

        } catch (Exception throwables) {
            throwables.printStackTrace();
            throw new dbException("Error DataBase connectION, Table User");
        }
    }
    /**
     * add Receipt to DataBase
     */
    public void addReceiptToBase(Receipt receipt) throws dbException {
        String SQL = "INSERT Receipt(price, status, volume, address, arrive_date, user_id, rout_id, weight, description) VALUE ('" + receipt.getPrice() + "', '" + receipt.getStatus() + "', '" + receipt.getVolume() + "', '" + receipt.getAddress() + "', '" + receipt.getArrive_date() + "', '" + receipt.getUser_id() + "', '" + receipt.getRout_id() + "', '"+ receipt.getWeight() +"', '" + receipt.getDescription() + "')";
        try {
            Connection connection = DriverManager.getConnection(connectionUrl, LOGIN, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(SQL);
            statement.execute();
            statement.close();
            connection.close();

        } catch (Exception throwable) {
            throwable.printStackTrace();
            throw new dbException("Error DataBase connectION, Table User");
        }
    }

    /**
     * get Receipt List from DataBase
     *@return list
     *        receipt List
     */
    public List<Receipt> getReceiptList() throws dbException {
        List<Receipt> list = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(connectionUrl, LOGIN, PASSWORD);
            Statement statement = connection.createStatement();
            ResultSet res = statement.executeQuery("SELECT * FROM finalproject.receipt;");
            while (res.next()) {
                list.add(new Receipt(res.getInt(1), res.getString(2), res.getString(3),res.getInt(4), res.getString(5),  res.getString(6), res.getInt(7), res.getInt(8) ,res.getString(9),res.getString(10)));
            }
            res.close();
            statement.close();
            connection.close();
        } catch (Exception throwable) {
            throwable.printStackTrace();
            throw new dbException("Error DataBase connectION, Table Receipt");
        }
        return list;
    }
    /**
     * update receipt status
     * @param id_receipt
     *        id receipt
     */
    public void updateStatus(int id_receipt, String newStatus) throws dbException {
        String SQL = "UPDATE receipt SET status='"+ newStatus +"' WHERE id="+id_receipt+";";
        try {
            Connection connection = DriverManager.getConnection(connectionUrl, LOGIN, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(SQL);
            statement.execute();
            statement.close();
            connection.close();

        } catch (Exception throwable) {
            throwable.printStackTrace();
            throw new dbException("Error DataBase connectION, Table Receipt");
        }
    }


}
